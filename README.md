# [g4music](https://gitlab.gnome.org/neithern/g4music) Packages

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-g4music.asc https://packaging.gitlab.io/g4music/gpg.key
```

## Add repo to apt

```bash
. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/g4music/$DISTRIB_CODENAME $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/morph027-g4music-$DISTRIB_CODENAME.list
```

## Install packages

```bash
sudo apt-get update
sudo apt-get install g4music morph027-keyring
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
echo 'Unattended-Upgrade::Origins-Pattern {"origin=morph027,codename=${distro_codename},label=g4music";};' | sudo tee /etc/apt/apt.conf.d/50g4music
```
