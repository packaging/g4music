#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

. /etc/lsb-release

export DEBIAN_FRONTEND=noninteractive
export SCCACHE_DIR="${CI_PROJECT_DIR}/sccache"
export RUSTC_WRAPPER=/usr/bin/sccache
mkdir -p "${SCCACHE_DIR}"
name="g4music"
repo="https://gitlab.gnome.org/neithern/g4music"
description="A beautiful, fast, fluent, light weight music player written in GTK4."
tag="${CI_COMMIT_TAG:-v0.0.0+0}"
version="${tag%%+*}"
tmpdir="$(mktemp -d)"
nfpm="${NFPM:-2.35.3}"
architecture="$(dpkg --print-architecture)"
case "${architecture}" in
    "amd64")
        goarch="x86_64"
        ;;
esac
apt-get update
apt-get \
    -y \
    --no-install-recommends \
    install \
    appstream-util \
    ca-certificates \
    ccache \
    curl \
    desktop-file-utils \
    gcc \
    gettext \
    git \
    libadwaita-1-dev \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer1.0-dev \
    libgtk-4-bin \
    meson \
    sccache \
    valac
rm -rf "${name}"
git clone -b "${version}" "${repo}"
cd "${name}"
meson setup build --buildtype=release --prefix /usr
meson install -C build --destdir "${tmpdir}"
mkdir -p "${CI_PROJECT_DIR}/${CODENAME}"
if ! command -v nfpm >/dev/null 2>&1; then
    curl -L \
        "https://github.com/goreleaser/nfpm/releases/download/v${nfpm}/nfpm_${nfpm}_Linux_${goarch}.tar.gz" |
        tar -xzf - -C /usr/bin nfpm
fi
cat >"${tmpdir}/nfpm.yaml" <<EOF
name: "${name}"
arch: ${architecture}
version: ${tag//[a-zA-Z]/}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: ${description}
homepage: ${repo}
contents:
  - src: ${tmpdir}/usr/
    dst: /usr
    type: tree
scripts:
  preinstall: ${CI_PROJECT_DIR}/.packaging/preinstall.sh
recommends:
  - morph027-keyring
depends:
  - libgtk-4-bin
EOF
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
mkdir -p "${CI_PROJECT_DIR}/${DISTRIB_CODENAME}"
cp -v ./*.deb "${CI_PROJECT_DIR}/${DISTRIB_CODENAME}/"
